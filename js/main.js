$(document).ready(function () {
    // Inline popups
    $('.section-profits__row-cell-price, .btn-credit, .services__block, .advantages, .header_callback , .section-profits__block-profit-button, .showModal').magnificPopup({
        type: 'inline',
        midClick: true,
        fixedContentPos: true,
        focus: '.modal__input',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });

    $('.section-footer__agree').magnificPopup({
        type: 'inline',
        midClick: true,
        fixedContentPos: true,
        focus: '.modal__input',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });

    $(document).on('click', '.calc__price-btn', function () {
        $.magnificPopup.open({
            items: {
                src: '#modalWindow11',
                type: 'inline'
            },
            midClick: true,
            fixedContentPos: true,
            focus: '.modal__input',
            removalDelay: 300,
            focus: 'input',
            mainClass: 'mfp-fade'
        });
    });

    $(document).on('click', '.calc__price-container-gift-block', function () {
        $.magnificPopup.open({
            items: {
                src: '#modalWindow12',
                type: 'inline'
            },
            midClick: true,
            fixedContentPos: true,
            focus: '.modal__input',
            removalDelay: 300,
            focus: 'input',
            mainClass: 'mfp-fade'
        });
    });

});


$('#begunok1').click(function () {
    $(this).hide('slow');
});


$("[name='openForm']").click(function () {

    //Вставляем img авто и его название в модальное окно
    if ($('.modal__success').hasClass('hidden')) {
        $('.modal-title').text('Отправьте заявку и получите выгоду ').append('<span class="nowrap">до ' + $(this).attr('data-discount') + ' руб.</span>')
    }
    else {
        $('.modal-title').text('Ваша заявка была успешно отправлена')
    }
    ;

    //В hidden input вставляем значение нажатой кнопки
    $("[name='clientButton']").val(this.getAttribute('data-val'));
    $("[name='complectationName']").val(this.getAttribute('data-complectation'));
    $("[name='modelName']").val(this.getAttribute('data-model'));
    $("[name='dName']").val(this.getAttribute('data-d'));


});


$('.section-profits__color-picker').click(function () {
    $(this).siblings('.section-profits__row-cell-color-img').attr("src", "img/hyundai-imgs/" + this.getAttribute('data-model') + "-" + this.getAttribute('data-color') + '.png');
    $(this).siblings('.section-profits__color-picker').removeClass('section-profits__color-picker-active');
    $(this).addClass('section-profits__color-picker-active');
});

$(".section-profits__img").on("click", function (e) {

    $(this).closest(".model-selector").toggleClass("active").siblings(".model-selector").removeClass("active");

    $("[name='car']").val(this.getAttribute('data-model'));

//получаем все описания моделей
    var modelInfo = $("#model_info").children();
    for (var i = 0; i < modelInfo.length; i++) {

        //если модель равна активной то показываем инфо по модели
        if ($(this).attr("data-model") == $(modelInfo[i]).attr("id") && $(this).closest(".model-selector").hasClass("active")) {

            $(modelInfo[i]).removeClass("hidden");

            $(modelInfo[i]).find('.section-profits__row-cell-color-img').each(function () {
                this.src = $(this).data('src');
                $(this).removeAttr('data-src');
            });

            ($(window).width() < 768) ?
                $('html,body').animate({scrollTop: $(modelInfo[i]).offset().top + 10}, 'slow')
                : $('html,body').animate({scrollTop: $(modelInfo[i]).offset().top - 55}, 'slow');

        } else {
            //иначе скрываем div
            $(modelInfo[i]).addClass("hidden");
        }
    }
});

$('.section-profits__avail-wrap').each(function () {
    if ($(this).attr('data-count') == 0) {
        id = this.getAttribute('data-id');
        $('button[data-id="' + id + '"]').each(function () {
            $(this).hide();
        });
    }
});

function phoneZoom() {
    $('.section-header__info-phone-icon').addClass('animated tada')
        .delay(2000)
        .queue(function () {
            $(this).removeClass('animated tada');
            $(this).dequeue();
        });
}

function goZoom() {
    setInterval(function () {
            phoneZoom();
        },
        3000
    );
}

goZoom();

$(".section-header__info-address").click(function () {
    $('html,body').animate({scrollTop: $('.section-contacts__page-heading').offset().top - 80}, 'slow');
});

$(function ($) {

    $('[name="clientPhone"]').mask('+7 (999) 999-9999');

    $(".calc__price-container-gift-block, .calc__price-btn").click(function () {
        $("[name='clientButton']").val(this.getAttribute('data-val'));
    });

    $(".formButton").click(function () {
        $("[name='clientButton']").val(this.getAttribute('data-val'));
    });

    $('.model-selector').click(function () {
        $("[name='modelName']").val(this.getAttribute('data-model'));
    });


    $('.section-profits__row-cell-btn').click(function () {
        $("[name='clientButton']").val(this.getAttribute('data-val'));

        //Записываем в скрытый input модель выбранного авто
        $("[name='complectationName']").val(this.getAttribute('data-complectation'));

    });

    $('.section-profits__avail-btn').click(function () {
        $("[name='complectationVin']").val(this.getAttribute('data-vin'));
    });
});

$(document).ready(function () {
    $("[name='form']").submit(function () {

        $('form').hide('slow').delay(5000).show('slow');
        $('.modal__success').show('slow').delay(5000).hide('slow');

        var str = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "php/mail.php",
            data: str,
            success: function (msg) {
                $(document).ajaxComplete(function () {
                    if (msg == "Спасибо! Ваша заявка принята.") {
                    }
                });
            }
        });
        return false;
    });
});